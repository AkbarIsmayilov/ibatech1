import React , {Component} from 'react';
import './ModalWindow.scss';
import PropTypes from 'prop-types';
import PageBtn from "../PageBtn/PageBtn";



class ModalWindow extends Component {
    constructor(props) {
        super(props);
        console.log( );
    }
    render() {
        return (

                <div className='window' >
                    <div className="window-header">{this.props.header}{ this.props.closeButton ? <div   className="close-btn">X</div> : null }</div>
                    <div className="window-body"><p className='window-body-text'>{this.props.text}</p>
                        <div className='window-btn-wrapper'>{this.props.actions}</div></div>
                </div>

        );
    }
}

ModalWindow.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.arrayOf(PropTypes.instanceOf(PageBtn))
}

export default ModalWindow;