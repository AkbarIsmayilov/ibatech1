import React, {Component} from 'react';
import './App.css';
import CardList from './components/CardList/CardList';
import PageBtn from "./components/PageBtn/PageBtn";
import ModalWindow from "./components/ModalWindow/ModalWindow";


class App extends Component {
    state = {
        cards: []
    };


    componentDidMount() {
    fetch("./cartInfo.json")
        .then(res =>res.json())
        .then(cardInfo =>
        this.setState({
            cards: [...cardInfo],
            isModalOpen: false
        })
        )
    }

    closeModalWindow = (e) => {
        if (e.target.classList.contains("window-wrapper")  || e.target.classList.contains("close-btn") ){
                this.setState({
                isModalOpen:false
            })
            console.log("hiiiiiiiii")
        }
    };

    addButtonHandler = () => {
      this.setState({
          isModalOpen: true
      })
    }

    render() {


        const windowButtons1 = [<PageBtn btnClass={'passed-buttons1'}
                                         style={"red"}
                                         text={"Ok"}
                                         btnHandler={this.exitWindow}/>,
            < PageBtn
                btnClass={'passed-buttons1'}
                style={"red"}
                text={"Cancel"}
                btnHandler={this.exitWindow}/>];


        return (
            <>
                { this.state.isModalOpen ? <ModalWindow closeWindowHandler={this.closeModalWindow} header={"Do you want add this card to carts ? "}  closeButton={true} actions={windowButtons1} text={"annnanannanananna"} /> :null



                }

                <div className="App">
                    {this.state.cards.length > 0 ? <CardList addCardHandler={this.addButtonHandler} cards={this.state.cards}/> : null}
                </div>
            </>
        );
    }
}

export default App;