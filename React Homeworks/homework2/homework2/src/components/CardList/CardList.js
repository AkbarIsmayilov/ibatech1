import React, {Component} from 'react';
import Card from "./Card/Card";
import './CardList.scss';



class CardList extends Component {
    constructor(props) {
        super(props);

    }
    cards = [...this.props.cards];

    cardContent = this.cards.map((item, index) => <Card addCardHandler={this.props.addCardHandler} key={index} self={item} id={index}  />  );
// console.log(index)
    render() {
        return (
            <div className={"cardList-container"}>
                {this.cardContent}
            </div>
        );
    }
}

export default CardList;