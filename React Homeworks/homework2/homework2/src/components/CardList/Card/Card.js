import React, {Component, createRef} from 'react';
import "./Card.scss"
class Card extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isStarActive: null

        }
    }


    starIcon =  createRef();

    cardNumber =  "isCard"+ this.props.id+"Active";

    toggleStar = () => {


     if ( JSON.parse(localStorage.getItem(this.cardNumber))=== null ) {
         localStorage.setItem(this.cardNumber, "true")
     }else{
         localStorage.setItem(this.cardNumber, JSON.stringify(!this.state.isStarActive) )
         this.setState({
             isStarActive: JSON.parse(localStorage.getItem(this.cardNumber))
         })
     }



    }


    componentDidMount() {
        this.setState({
            isStarActive: JSON.parse(localStorage.getItem(this.cardNumber))
        })
    }

    render() {
        const {name ,imgSrc , price } = this.props.self ;
        return (
            <div className={"card-container"}>
                <img src={imgSrc} alt="" className="card-img"/>
                <div className="card-info">
                    <div className="card-header">
                        <h1 className="card-header-title">{name}</h1>
                        <p className="card-header-author">by Artist</p>
                    </div>
                    { this.state.isStarActive ? <img onClick={this.toggleStar} ref={this.starIcon} src={"./img/album_images/star@1X (2).png"} /> :
                    <img onClick={this.toggleStar} ref={this.starIcon} src={"./img/album_images/star@1X (3).png"} />}
                    <p className="card-description"><span className={"card-description-fancy"}>Lorem ipsum</span> dolor sit amet, consectetur adipisicing elit. Alias corporis cum cumque blah blah blah </p>
                    <div className="card-bottom">
                        <p className="card-product-price">$ {price}</p>
                        <button onClick={this.props.addCardHandler} className="card-button">add to cart</button>
                    </div>


                </div>
            </div>
        );
    }
}

export default Card;