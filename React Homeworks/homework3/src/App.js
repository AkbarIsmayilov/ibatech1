import React, {Component} from 'react';
import './App.css';
import CardList from './components/CardList/CardList';
import PageBtn from "./components/PageBtn/PageBtn";
import ModalWindow from "./components/ModalWindow/ModalWindow";
import {Switch, Route, Link} from "react-router-dom";
import Favourite from "./components/Favourite/Favourite";
import Cart from "./components/Cart/Cart";

class App extends Component {
  state = {
    cards: []
  };


  componentDidMount() {
    fetch("./cartInfo.json")
        .then(res =>res.json())
        .then(cardInfo =>
            this.setState({
              cards: [...cardInfo],
              isModalOpen: false
            })
        )
  }

  closeModalWindow = (e) => {
    if (e.target.classList.contains("window-wrapper")  || e.target.classList.contains("close-btn") || e.target.textContent === 'Cancel' ){
      this.setState({
        isModalOpen:false
      })}
  };

  addToCartHandler = (e) => {
      if (e.target.textContent === 'Ok') {
          let cardToAddToCart = 'isCard'+JSON.parse(localStorage.getItem('currentCardToAddCart'))+'InCart';
          localStorage.setItem(cardToAddToCart, true);
          this.setState({
              isModalOpen:false
          })
      }

  };

  addButtonHandler = (e) => {
      localStorage.setItem("currentCardToAddCart", e.target.id );
      this.setState({
          isModalOpen: true
      })

  }



  render() {


    const windowButtons1 = [  <PageBtn
                                    text={"Ok"}/>,
                              < PageBtn
                                      text={"Cancel"}
                                      />];


    return (
        <>

          <div className="home-page-top-bar-wrapper">
              <div className="home-page-top-bar">
                  <Link className={'link-style'} to={'/CardList'}>Card List </Link>
                  <Link  className={'link-style'} to={'/Cart'}>Cart </Link>
                  <Link className={'link-style'}  to={'/Favourite'}>Favourite </Link>
              </div>
          </div>

          {       this.state.isModalOpen ?
              <ModalWindow
              addToCartHandler={this.addToCartHandler}
              closeWindowHandler={this.closeModalWindow}
              header={"Do you want add this card to carts ? "}
              closeButton={true} actions={windowButtons1}
              text={"annnanannanananna"} /> :null}

          <Switch>
              <Route path={'/CardList'}>
                  {this.state.cards.length > 0 ?
                      <CardList
                      addCartHandler={this.addButtonHandler}
                      cards={this.state.cards}/> : null}
              </Route>
              <Route path={'/Favourite'}>
                  {this.state.cards.length > 0 ?
                      <Favourite
                      addCartHandler={this.addButtonHandler}
                      cards={this.state.cards}/> : null}
              </Route>
              <Route path={'/Cart'}>
                  { this.state.cards.length > 0 ?
                      < Cart
                      addCartHandler={this.addButtonHandler}
                      cards={this.state.cards}/> : null  }
              </Route>
          </Switch>
        </>
    );
  }
}

export default App;