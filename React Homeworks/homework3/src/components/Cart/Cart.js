import React, {Component} from 'react';
import Card from "../Card/Card";
import './Cart.scss';
import '../../App.css'



class Cart extends Component {
    constructor(props) {
        super(props);
    }
    cards = [...this.props.cards];

    cardContent = [];

    rerenderParentCallBack() {
        this.forceUpdate()
    }



    deleteFromCartHandler = (e) => {
        console.log("deleted");
        localStorage.setItem("currentCardToCart", e.target.id );

            let cardToAddToCart = 'isCard'+e.target.id+'InCart';
            localStorage.setItem(cardToAddToCart, false);
            this.forceUpdate();

    } ;


    // cardContent = this.cards.map((item, index) => <Card  addCartHandler={this.props.addCartHandler}key={index} self={item} id={index}  />  );
    render() {
        this.cards.forEach( item => {
            let cardNumber = "isCard"+item.id+"InCart";
            if(JSON.parse(localStorage.getItem(cardNumber))) {
                this.cardContent.push(<Card deleteFromCartHandler={this.deleteFromCartHandler} isDeleteable={true} isAddBtnEnable={false}  rerenderParentCallBack={this.rerenderParentCallBack.bind(this)} addCartHandler={this.props.addCartHandler}  key={item.id} self={item} id={item.id} />)
            }
        });

        return (
            <div className="App" >
                <div className={"cardList-container"} >
                    {this.cardContent}

                </div>
                {this.cardContent = []}
            </div>
        );
    }
}

export default Cart;