import React , {Component} from 'react';
import './ModalWindow.scss';
import PropTypes from 'prop-types';
import PageBtn from "../PageBtn/PageBtn";



class ModalWindow extends Component {
    constructor(props) {
        super(props);
    }



    render() {
        const {header, closeButton ,actions, text } = this.props;
        return (
    <div onClick={this.props.closeWindowHandler} className="window-wrapper" >
            <div onClick={this.props.addToCartHandler} className='window' >
                <div className="window-header">{header}{closeButton ? <div  onClick={this.props.closeWindowHandler} className="close-btn">X</div> : null }</div>
                <div className="window-body"><p className='window-body-text'>{text}</p>
                    <div className='window-btn-wrapper'>{actions}</div>
                </div>
            </div>
    </div>
        );
    }
}

ModalWindow.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.arrayOf(PropTypes.instanceOf(PageBtn))
}

export default ModalWindow;