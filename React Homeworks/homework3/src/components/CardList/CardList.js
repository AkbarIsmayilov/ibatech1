import React, {Component} from 'react';
import Card from "../Card/Card";
import './CardList.scss';
import '../../App.css'
import {useLocation} from "react-router-dom";


class CardList extends Component {
    constructor(props) {
        super(props);


    }
    cards = [...this.props.cards];


    cardContent = this.cards.map((item, index) => <Card  addCartHandler={this.props.addCartHandler} isAddBtnEnable={true} key={index} self={item} id={index}  />  );

    render() {


        return (
            <div className="App">
                <div className={"cardList-container"}>
                    {this.cardContent}
                 </div>
            </div>
        );
    }
}

export default CardList;