import React, {Component, createRef} from 'react';
import "./Card.scss"
import {useLocation} from "react-router-dom";
class Card extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isStarActive: null,

        }
    }


    cardNumber =  "isCard"+ this.props.id+"Active";


    toggleStar = () => {
        if ( JSON.parse(localStorage.getItem(this.cardNumber))=== null ) {
            localStorage.setItem(this.cardNumber, "true")
         }else{
             localStorage.setItem(this.cardNumber, JSON.stringify(!this.state.isStarActive) )
             this.setState({
                 isStarActive: JSON.parse(localStorage.getItem(this.cardNumber))
              })
        }
            if(window.location.pathname === "/Favourite" ) {
                this.props.rerenderParentCallBack();
        }


    }


    componentDidMount() {
        this.setState({
            isStarActive: JSON.parse(localStorage.getItem(this.cardNumber))
        })
    }

    render() {

        const {name ,imgSrc , price } = this.props.self ;
        return (
            <div className={"card-container"}>

                <div className="card-img-wrapper">
                    { this.props.isDeleteable ? <div  onClick={this.props.deleteFromCartHandler} className={"card-delete-btn"}>
                        <img id={this.props.id} src="./img/album_images/remove-icon.png" alt = "" className="card-delete-icon"/></div> : null}
                    <img src={imgSrc} alt="album photo" className="card-img"/>
                </div>
                <div className="card-info">
                    <div className="card-header">
                        <h1 className="card-header-title">{name}</h1>
                        <p className="card-header-author">by Artist</p>
                    </div>
                    { this.state.isStarActive ? <img onClick={this.toggleStar} ref={this.starIcon} src={"./img/album_images/star@1X (2).png"} /> :
                    <img onClick={this.toggleStar} ref={this.starIcon} src={"./img/album_images/star@1X (3).png"} />}
                    <p className="card-description"><span className={"card-description-fancy"}>Lorem ipsum</span> dolor sit amet, consectetur adipisicing elit. Alias corporis cum cumque blah blah blah </p>
                    <div className="card-bottom">
                        <p className="card-product-price">$ {price}</p>
                        {this.props.isAddBtnEnable ? <button id={this.props.id} onClick={this.props.addCartHandler} className="card-button">add to cart</button> : null}
                    </div>


                </div>
            </div>
        );
    }
}

export default Card;