import React, {Component} from 'react';
import Card from "../Card/Card";
import './Favourite.scss';
import '../../App.css'



class Favourite extends Component {
    constructor(props) {
        super(props);

    }
    cards = [...this.props.cards];
    cardContent = [];



    rerenderParentCallBack() {
        this.forceUpdate()
    }



    render() {
    this.cards.forEach( item => {
        let cardNumber = "isCard"+item.id+"Active";
        if(JSON.parse(localStorage.getItem(cardNumber))) {
            this.cardContent.push(<Card rerenderParentCallBack={this.rerenderParentCallBack.bind(this)} addCartHandler={this.props.addCartHandler} isAddBtnEnable={true} key={item.id} self={item} id={item.id} />)
        }
    })

        return (
            <div className="App">
                <div className={"cardList-container"}>
                    {this.cardContent}
                    {this.cardContent = []}

                </div>
            </div>
        );

    }
}

export default Favourite;